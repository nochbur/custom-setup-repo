# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="robbyrussell"

# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# Set to this to use case-sensitive completion
# CASE_SENSITIVE="true"

# Uncomment this to disable bi-weekly auto-update checks
# DISABLE_AUTO_UPDATE="true"

# Uncomment to change how often before auto-updates occur? (in days)
# export UPDATE_ZSH_DAYS=13

# Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want to disable command autocorrection
# DISABLE_CORRECTION="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
# COMPLETION_WAITING_DOTS="true"

# Uncomment following line if you want to disable marking untracked files under
# VCS as dirty. This makes repository status check for large repositories much,
# much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment following line if you want to  shown in the command execution time stamp 
# in the history command output. The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|
# yyyy-mm-dd
# HIST_STAMPS="mm/dd/yyyy"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(git copydir copyfile composer symfony2 supervisor)

source $ZSH/oh-my-zsh.sh

# User configuration

export PATH="/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin"
# export MANPATH="/usr/local/man:$MANPATH"

# # Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

###########################################################
###########################################################
####################### CUSTOM ############################

export PATH="$PATH:/usr/local/mysql/bin"
export PATH=~/bin:$PATH
#export PATH=/Applications/GoogleAppEngineLauncher.app/Contents/Resources/GoogleAppEngine-default.bundle/Contents/Resources/google_appengine/:$PATH
export MANPATH=/opt/local/share/man:$MANPATH
export C_INCLUDE_PATH=/usr/local/include
export CPLUS_INCLUDE_PATH=/usr/local/include
export LIBRARY_PATH=/usr/local/lib
export CLICOLOR=1
export LSCOLORS=ExFxCxDxBxegedabagacad
export JAVA_HOME=`/usr/libexec/java_home`

# settings
export LC_CTYPE="de_AT.UTF-8"
export LC_MESSAGES="en_US.UTF-8"

export PATH=~/bin:/opt/local/bin:/opt/local/sbin:$PATH
export MANPATH=/opt/local/share/man:$MANPATH
export EDITOR=/usr/bin/vi

#aws stuff
export EC2_HOME=~/opt/ec2/
export PATH=$EC2_HOME/bin:$PATH
export AWS_AUTO_SCALING_HOME=~/opt/autoscale/
export PATH=$AWS_AUTO_SCALING_HOME/bin:$PATH

export EC2_PRIVATE_KEY=~/.ec2/pk-UYTO3BDKSKZSXF3U3NBQVLXKTDER3X2S.pem
export EC2_CERT=~/.ec2/cert-UYTO3BDKSKZSXF3U3NBQVLXKTDER3X2S.pem
export AWS_CLOUDWATCH_HOME=~/opt/ec2

#php infos
export PATH="$(brew --prefix php54)/bin:$PATH"

#source ~/lib/git-completion.sh
#source ~/lib/git-flow-completion.sh

# Customize to your needs...
export PATH=/usr/local/bin:$PATH:/usr/bin:/bin:/usr/sbin:/sbin:~/bin
export PATH=/usr/local/sbin:$PATH

#custom settings
export HISTSIZE=5000
export HISTCONTROL=ignoreboth
ulimit -n 2048

# po edit
export POEDITOR_API_TOKEN=4ef6b9959baaf8407c5585ad80f82f5e

#aliases
alias server-start="py27 -m SimpleHTTPServer"

# Sulu web console
compdef _symfony2 app/console
compdef _symfony2 app/webconsole

# Load zsh-syntax-highlighting.
source ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Load zsh-autosuggestions.
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh

# Enable autosuggestions automatically.
zle-line-init() {
    zle autosuggest-start
}
zle -N zle-line-init
bindkey '^G' autosuggest-toggle

######################################################################

#eval "$(pyenv init -)"
