export PATH="$PATH:/usr/local/mysql/bin"
export PATH=~/bin:$PATH
export PATH=/Applications/GoogleAppEngineLauncher.app/Contents/Resources/GoogleAppEngine-default.bundle/Contents/Resources/google_appengine/:$PATH
export MANPATH=/opt/local/share/man:$MANPATH
export C_INCLUDE_PATH=/usr/local/include
export CPLUS_INCLUDE_PATH=/usr/local/include
export LIBRARY_PATH=/usr/local/lib
export CLICOLOR=1
export LSCOLORS=ExFxCxDxBxegedabagacad
export PS1='\[\033[01;32m\]\u:\[\033[01;33m\]\w$(__git_ps1 " (%s)")\$ \[\033[00m\]'
export JAVA_HOME=`/usr/libexec/java_home`

# settings
export LC_CTYPE="de_AT.UTF-8"
export LC_MESSAGES="en_US.UTF-8"

export PATH=~/bin:/opt/local/bin:/opt/local/sbin:$PATH
export MANPATH=/opt/local/share/man:$MANPATH
export EDITOR=/usr/bin/vi

#export JAVA_HOME=/System/Library/Frameworks/JavaVM.framework/Home
export EC2_PRIVATE_KEY=~/.ec2/pk-UYTO3BDKSKZSXF3U3NBQVLXKTDER3X2S.pem
export EC2_CERT=~/.ec2/cert-UYTO3BDKSKZSXF3U3NBQVLXKTDER3X2S.pem
export AWS_CLOUDWATCH_HOME=~/opt/ec2

#source ~/lib/git-completion.sh
#source ~/lib/git-flow-completion.sh

##eval `keychain -q --eval --agents ssh --inherit any id_dsa`

#custom settings
export HISTSIZE=5000
export HISTCONTROL=ignoreboth
