;; ========= KEYS - SHORT CUTS =================================================
;; y and n shorthand
(fset 'yes-or-no-p 'y-or-n-p)

;; define various key bindings
;; F9 for speedbar
(global-set-key [(control f9)] 'speedbar)
(global-set-key [f7] 'ispell-word)

(global-set-key [(alt left)] 'previous-buffer)
(global-set-key [(alt right)] 'next-buffer)
(global-set-key [(control shift k)] 'bd-kill-ring-save-line)
;;comment uncomment
(global-set-key [(control q)] 'comment-or-uncomment-region)
(global-set-key [(control shift alt m)] 'uncomment-region)
(global-set-key [(control .)] 'dabbrev-expand)
(global-set-key [(control tab)] 'dabbrev-expand)
(global-set-key [(alt n)] 'make-frame-command)
;;jump to line
(global-set-key [(alt l)] 'goto-line)
;;replace-string
(global-set-key [(alt %)] 'replace-string)
;;intent-region
(global-set-key [(alt i)] 'indent-region)
;;get minibuffer
(global-set-key [(alt y)] 'execute-extended-command)

(global-set-key [(alt shift up)] 'windmove-up)
(global-set-key [(alt shift down)] 'windmove-down)
(global-set-key [(alt shift left)] 'windmove-left)
(global-set-key [(alt shift right)] 'windmove-right)

(global-set-key [(meta meta)] 'keyboard-quit)

(global-set-key [(alt shift c)] 'pastie-buffer)

(global-set-key "\d" 'delete-backward-char)

(global-set-key [f4]
  #'(lambda ()
      (interactive)
      (move-beginning-of-line nil)
      (insert "import pdb; pdb.set_trace()")
      (indent-for-tab-command)
      (insert "\n")))

(global-set-key [f3]
  #'(lambda ()
      (interactive)
      (move-beginning-of-line nil)
      (insert "from lovely.gae.environment import BREAKPOINT; BREAKPOINT()")
      (indent-for-tab-command)
      (insert "\n")))

(global-set-key [f5] 'shell)
(add-hook 'shell-mode-hook
	  (function (lambda ()
		      (setq comint-scroll-to-bottom-on-input t)
		      (setq comint-scroll-to-bottom-on-output t)
		      (setq comint-scroll-show-maximum-output t)
		      (compilation-shell-minor-mode))))

(global-set-key [(alt shift f)] 'grep-find)
