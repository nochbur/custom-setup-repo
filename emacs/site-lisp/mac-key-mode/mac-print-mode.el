;;; mac-print-mode.el --- Cocoa print dialog for Carbon Emacsen

;; Copyright (C) 2005-2006  Seiji Zenitani <zenitani@mac.com>

;; based on htmlize-view by Lennart Borgman
;; http://ourcomments.org/Emacs/DL/elisp/htmlize-view.el

;;; Commentary:

;; In order to use this package,
;; htmlize.el <http://fly.srk.fer.hr/~hniksic/emacs/htmlize.el>
;; and coral 1.1 or later <http://hmdt-web.net/coral/> are required.
;;
;;
;; M-x mac-print-buffer
;; or
;; (mac-print-mode 1)
;;

;;; Code:

(require 'htmlize)

(defvar mac-print-kill-view-buffers t
  "If non-nil, delete temporary html buffers after sending to coral.")

(defvar mac-print-coral-program "coral" "The coral program.")


;;;###autoload
(defun mac-print-buffer(&optional region-only)
  "Convert buffer to html, preserving colors and decoration and
send it to the coral application.
If REGION-ONLY is non-nil then only the region is printed."
  (interactive)
  (shell-command
   (concat mac-print-coral-program " -d "
           (mac-print-htmlize-buffer-to-tempfile region-only)
           )))

(defun mac-print-htmlize-buffer-to-tempfile(region-only)
  "Convert buffer to html, preserving colors and decoration and
send it to the coral application.
If REGION-ONLY is non-nil then only the region is sent to the coral.
Return a cons with temporary file name followed by temporary buffer."
  (save-excursion
    (let (;; Just use Fundamental mode for the temp buffer
          magic-mode-alist
          auto-mode-alist
          (html-temp-buffer
           (if (not region-only)
               (htmlize-buffer (current-buffer))
             (let ((start (mark)) (end (point)))
               (or (<= start end)
                   (setq start (prog1 end (setq end start))))
               (htmlize-region start end))))
          (file (mac-print-gettemp-file-name)))
      (set-buffer html-temp-buffer)
      (write-file file nil)
      (if mac-print-kill-view-buffers (kill-buffer html-temp-buffer))
      file)))

(defun mac-print-gettemp-file-name()
  "Get a temp file name for printing"
  (make-temp-file
   (expand-file-name
    "mac-print"
    (if (featurep 'carbon-emacs-package) (carbon-emacs-package-tmpdir) "/tmp")
    ) nil ".html"))


(defadvice ps-print-buffer-with-faces
  (around mac-print-ad())
  (mac-print-buffer))
(defadvice ps-print-region-with-faces
  (around mac-print-ad())
  (mac-print-buffer t))
(defadvice ps-print-buffer
  (around mac-print-ad())
  (mac-print-buffer))
(defadvice ps-print-region
  (around mac-print-ad())
  (mac-print-buffer t))
(defadvice print-buffer (around mac-print-ad2(&optional mono))
  (interactive "P")
  (mac-print-buffer))
(defadvice print-region (around mac-print-ad2(&optional mono))
  (interactive "P")
  (mac-print-buffer t))

;;;###autoload
(define-minor-mode mac-print-mode
  "Toggle Mac Print mode."
  :global t
  (if mac-print-mode
      (ad-enable-regexp "mac-print-ad*")
    (ad-disable-regexp "mac-print-ad*")
    ))


(provide 'mac-print-mode)

;; mac-print-mode.el ends here.
