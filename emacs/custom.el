;; ============= CUSTOM EMACS CONFIG ==============
(setq inhibit-startup-message   t)   ; Don't want any startup message
(setq make-backup-files         nil) ; Don't want any backup files
(setq auto-save-list-file-name  nil) ; Don't want any .saves files
(setq auto-save-default         nil) ; Don't want any auto saving

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(blink-cursor-mode t nil (frame))
 '(browse-url-netscape-program "mozilla")
 '(case-fold-search t)
 '(column-number-mode t)
 '(comint-input-autoexpand t)
 '(comint-input-ignoredups t)
 '(comint-move-point-for-output t)
 '(comint-prompt-read-only t)
 '(current-language-environment "English")
 '(ecb-options-version "2.32")
 '(exec-path
   (quote
    ("/usr/bin" "/bin" "/usr/sbin" "/sbin" "/Applications/Emacs.app/Contents/MacOS/libexec" "/Applications/Emacs.app/Contents/MacOS/bin" "/usr/local/bin" "/usr/X11R6/bin" "/opt/local/bin" "~/bin")))
 '(global-font-lock-mode t nil (font-lock))
 '(grep-command "grep -ir ")
 '(grep-find-command
   "find . -not -path \"*svn*\" -type f -print0 | xargs -0 grep -n -e ")
 '(jde-jdk (quote mac))
 '(mouse-wheel-mode t nil (mwheel))
 '(nxml-attribute-indent 4)
 '(nxml-child-indent 2)
 '(nxml-slash-auto-complete-flag t)
 '(package-selected-packages (quote (ac-html ack js2-mode json-mode load-dir)))
 '(rst-mode-lazy nil)
 '(safe-local-variable-values
   (quote
    ((hl-sexp-mode)
     (rainbow-mode . t)
     (encoding . utf-8))))
 '(scroll-bar-mode (quote right))
 '(show-paren-mode t)
 '(show-trailing-whitespace t)
 '(tool-bar-mode nil nil (tool-bar)))

;; Coding system UTF-8
(set-terminal-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-buffer-file-coding-system 'utf-8)

;; global formatting stuff
(setq transient-mark-mode t)
(setq fill-column 78)
(setq-default tab-width 4)
(setq-default indent-tabs-mode nil)
(global-font-lock-mode 2)
(setq show-paren-delay 0.4)
(show-paren-mode t)
(setq show-paren-style (quote expression))
(global-hl-line-mode 1)
;;(set-face-background 'hl-line "light yellow")

(setq-default truncate-lines t) ;;do not wrap long lines

;; time format
(setq display-time-24hr-format t)
(display-time)

;;emacs look on start up
(setq initial-frame-alist '
      ((width . 168)
       (height . 52)
       (top . 0)
       (left . 0)
       ))

(setq default-frame-alist
      '((top . 100) (left . 100)
        (width . 82) (height . 46)
        ))

(split-window-horizontally)

;; update timestamps when saving
(add-hook 'write-file-hooks 'time-stamp)

;;(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
;; '(default ((t (:stipple nil :background "white" :foreground "black" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 110 :width normal :family "apple-monaco")))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(web-mode-html-tag-face ((t (:foreground "gray77")))))
