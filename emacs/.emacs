;; define where the emacs files are located

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(add-to-list 'load-path "~/naba-config/emacs/")
(autoload 'ansi-color-for-comint-mode-on "ansi-color" nil t)
(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)

(setq uniquify-buffer-name-style 'post-forward)
(setq uniquify-separator " | ")
(setq uniquify-after-kill-buffer-p t)
(require 'uniquify)

(require 'recentf)
(setq recentf-auto-cleanup 'never) ;; disable before we start recentf!
(recentf-mode 1)

(message "Loading Philip's Emacs File")

;; =========== LOAD CUSTOM FILES ===============================================
(setq custom-file "~/naba-config/emacs/custom.el") ;; Custom Styles
(load custom-file 'noerror)            ; ignore if no exist
;; load sub-config files
(defconst user-init-dir
  (cond ((boundp 'user-emacs-directory)
         user-emacs-directory)
        ((boundp 'user-init-directory)
         user-init-directory)
        (t "~/naba-config/emacs")))

(defun load-user-file (file)
  (interactive "f")
  "Load a file in current user's configuration directory"
  (load-file (expand-file-name file user-init-dir)))

(load-user-file "shortcuts.el")
;; =============================================================================

;; =========== PLUGIN IMPORT ===================================================
(let* ((dir (expand-file-name "~/naba-config/emacs/site-lisp"))
       (default-directory dir))
  (when (file-directory-p dir)
    (add-to-list 'load-path dir)
    (if (fboundp 'normal-top-level-add-subdirs-to-load-path)
        (normal-top-level-add-subdirs-to-load-path))))

;;(require 'redo)

;;(load "python-mode.el")
(autoload 'python-mode "python-mode" "Python editing mode." t)
(autoload 'doctest-mode "doctest-mode" "Python Doctest editing mode." t)
(add-hook 'python-mode-hook (lambda() (auto-fill-mode 0)))
(add-hook 'doctest-mode-hook (lambda() (auto-fill-mode 0)))

;; php  mode
(require 'php-mode)

(copy-face 'default 'comint-highlight-prompt)
(set-face-foreground 'comint-highlight-prompt "firebrick1")

;; typescript-mode
(require 'typescript)

;; font-lock
(require 'font-lock)

;; Allow the cursor color to toggle depending on overwrite mode
(defvar cursor-color nil "Last cursor color used (string)")
(defun ins-cursor-set () "set cursor color according to ins mode"
       (let ((sv-crs-str cursor-color))
           (if overwrite-mode
               (setq cursor-color "black")        ;overwrite cursor color
               (setq cursor-color "red"))        ;insert mode
           (or (string= sv-crs-str cursor-color)
               (set-cursor-color cursor-color))))  ;X terminal cursor color
(add-hook 'post-command-hook 'ins-cursor-set)

;; Show the filename, and the buffer name in the title bar.
(setq frame-title-format (list "E:%f"))
(setq icon-title-format frame-title-format)

;; setting up ediff mode
(setq ediff-split-window-function 'split-window-horizontally)

;; map files to modes
;; nxml for xml
(load "rng-auto.el")
;; http://www.emacswiki.org/cgi-bin/wiki/NxmlMode
;; So, to make set-auto-mode respect 'auto-mode-alist', say:
;;(setq xml-based-modes (cons 'nxml-mode) nil);
(setq magic-mode-alist nil);
(setq magic-mode-alist (cons '("<\\?xml\\s " . nxml-mode) magic-mode-alist))
(setq magic-mode-alist (cons '("<html\\s " . nxml-mode) magic-mode-alist))
(setq magic-mode-alist (cons '("===+" . doctest-mode) magic-mode-alist))

;; But some xml files are not named nicely. To also force nxml-mode
;; for all documents with contents beginning "<\\?xml \\|<!DOCTYPE"
;; say:
(fset 'xml-mode 'nxml-mode)
(setq auto-mode-alist
      (append
       (list
        '("\\.dtml\\'" . sgml-mode)
        '("\\.py$" . python-mode)
        '("\\.html$" . nxml-mode)
        '("\\.twig$" . nxml-mode)
        '("\\.jinja2$" . nxml-mode)
        '("\\.zcml$" . nxml-mode)
        '("\\.php$" . php-mode)
        '("\\.sgm$" . sgml-mode)
        '("\\.sgml$" . sgml-mode)
        '("\\.xml$" . nxml-mode)
        '("\\.pt$" . nxml-mode)
        '("\\.zpt$" . nxml-mode)
        '("\\.fo" . nxml-mode)
        '("\\.xsl" . nxml-mode)
        '("\\.svg" . nxml-mode)
        '("\\.xul" . sgml-mode)
        '("\\.stx" . indented-text-mode)
        '("\\.txt" . doctest-mode)
        '("\\.js" . javascript-mode)
        '("\\.css" . css-mode)
        '("\\.less" . css-mode)
        '("\\.scss" . css-mode)
        '("\\.sass" . css-mode)
        '("\\.j" . objj-mode)
        '("\\.h" . objj-mode)
        '("\\.ts" . typescript-mode)
        '("\\.json" . json-mode)
        '("\\.md" . text-mode)
        ;;'("\\.po[tx]?" . po-mode)
        )
       auto-mode-alist)
      )

(require 'rst)
(setq auto-mode-alist
      (append '(("\\.rst$" . rst-mode)
                ("\\.rest$" . rst-mode)) auto-mode-alist))

(add-hook 'text-mode-hook 'rst-text-mode-bindings)
(add-hook 'rst-adjust-hook 'rst-toc-insert-update)

(require 'tramp)
(setq tramp-default-method "scp")

(add-to-list 'custom-theme-load-path
             (file-name-as-directory "~/naba-config/emacs/themes/"))
;;(add-to-list 'custom-theme-load-path
;;             (file-name-as-directory "~/naba-config/emacs/themes/farmhouse-theme"))

;; load your favorite theme
;;(load-theme 'high-contrast t)
;;(load-theme 'farmhouse-dark t)
(load-theme 'material t)

;; ========= EMACS PLUGINS ======================
;; located at: ~/naba-config/emacs/site-lisp

;;mac-alike-key mode
(require 'mac-key-mode)
   (mac-key-mode 1)
   (setq mac-option-modifier nil)
;;-------------------------------------------------------------------------------------
;;html plugin, to auto close html tags
(require 'tidy)
(auto-revert-mode 1)
;;-------------------------------------------------------------------------------------

;;-------------------------------------------------------------------------------------
;;html web template plugin for indent and highlighting of templates
(require 'web-mode)
(setq auto-mode-alist
      (append '(("\\.html$" . web-mode)
                ("\\.twig$" . web-mode)) auto-mode-alist))

(defun my-web-mode-hook ()
  "Hooks for Web mode."
  (setq web-mode-markup-indent-offset 4)
  ;;(set-face-attribute 'web-mode-html-tag-face nil :foreground "DarkGreen")
  (set-face-attribute 'web-mode-html-attr-name-face nil :foreground "brown2")
  (set-face-attribute 'web-mode-html-attr-value-face nil :foreground "RoyalBlue1")
  (setq web-mode-enable-auto-pairing t)
)
(add-hook 'web-mode-hook  'my-web-mode-hook)
;;-------------------------------------------------------------------------------------

;; js-lint to check javascript code
(defun jslint-thisfile ()
  (interactive)
  (compile (format "~/bin/jsl -conf ~/bin/jsl.default.conf -process %s" (buffer-file-name))))
;;-------------------------------------------------------------------------------------

;; autocomplete
(add-to-list 'load-path "~/naba-config/emacs/site-lisp/auto-complete-1.3.1")

; Load the default configuration
(require 'auto-complete-config)
; Make sure we can find the dictionaries
(add-to-list 'ac-dictionary-directories "~/naba-config/emacs/site-lisp/auto-complete-1.3.1/dict")
; Use dictionaries by default
(setq-default ac-sources (add-to-list 'ac-sources 'ac-source-dictionary))
(global-auto-complete-mode t)
; Start auto-completion after 2 characters of a word
(setq ac-auto-start 2)
; case sensitivity is important when finding matches
(setq ac-ignore-case nil)

(put 'set-goal-column 'disabled nil)
;;-------------------------------------------------------------------------------------

(setq load-path (cons "~/naba-config/emacs/site-lisp/geben-0.26" load-path))

(autoload 'geben "geben" "DBGp protocol frontend, a script debugger" t)

;; Debug a simple PHP script.
;; Change the session key my-php-54 to any session key text you like
(defun my-php-debug ()
  "Run current PHP script for debugging with geben"
  (interactive)
  (call-interactively 'geben)
  (shell-command
    (concat "XDEBUG_CONFIG='idekey=my-php-55-key' /usr/local/bin/php "
    (buffer-file-name) " &"))
  )

(global-set-key [f5] 'my-php-debug)

;;-------------------------------------------------------------------------------------
;;(require 'flymake)

;;(defun flymake-php-init ()
;;  "Use php to check the syntax of the current file."
;;  (let* ((temp (flymake-init-create-temp-buffer-copy 'flymake-create-temp-inplace))
;;	 (local (file-relative-name temp (file-name-directory buffer-file-name))))
;;    (list "php" (list "-f" local "-l"))))

;;(add-to-list 'flymake-err-line-patterns
;;  '("\\(Parse\\|Fatal\\) error: +\\(.*?\\) in \\(.*?\\) on line \\([0-9]+\\)$" 3 4 nil 2))

;;(add-to-list 'flymake-allowed-file-name-masks '("\\.php$" flymake-php-init))

;; Drupal-type extensions
;;(add-to-list 'flymake-allowed-file-name-masks '("\\.module$" flymake-php-init))
;;(add-to-list 'flymake-allowed-file-name-masks '("\\.install$" flymake-php-init))
;;(add-to-list 'flymake-allowed-file-name-masks '("\\.inc$" flymake-php-init))
;;(add-to-list 'flymake-allowed-file-name-masks '("\\.engine$" flymake-php-init))

(add-hook 'php-mode-hook (lambda () (flymake-mode 1)))
(define-key php-mode-map '[M-S-up] 'flymake-goto-prev-error)
(define-key php-mode-map '[M-S-down] 'flymake-goto-next-error)
;;-------------------------------------------------------------------------------------

;; Turn auto refresh on
(global-auto-revert-mode t)

;; Set default directory when starting emacs
(setq default-directory "~/sandbox/")

;; Anivo specifics
;;(setq js-indent-level 2)

;; Move to naba-config
(defun untabify-and-indent ()
  "Run `some-command' and `some-other-command' in sequence."
  (interactive)
  (untabify (point-min) (point-max))
  (indent-region (point-min) (point-max)))

(global-set-key (kbd "A-<") 'untabify-and-indent)

(defun add-block-comment-and-indent ()
  (interactive)
  (move-beginning-of-line nil)
  (insert "/**")
  (indent-for-tab-command)
  (insert "\n")
  (insert "*")
  (indent-for-tab-command)
  (insert "\n")
  (insert "*/")
  (indent-for-tab-command)
  (insert "\n")
  (indent-region (point-min) (point-max)))

(global-set-key (kbd "A-+") 'add-block-comment-and-indent)

(message "Done Loading Emacs File")
