" =============================================================================
" VIM UI Look
" =============================================================================
colorscheme scheakur
set lines=60
set columns=180
set background=dark

" =============================================================================
" VIM Config
" =============================================================================
set nocompatible               " Be iMproved

set ignorecase

set ruler

set encoding=utf-8

" Note: Skip initialization for vim-tiny or vim-small.
if 0 | endif

if has('vim_starting')
  if &compatible
    set nocompatible               " Be iMproved
  endif

  " Required:
  set runtimepath+=~/.vim/bundle/neobundle.vim/
endif

" Required:
call neobundle#begin(expand('~/.vim/bundle/'))

" Let NeoBundle manage NeoBundle
" Required:
NeoBundleFetch 'Shougo/neobundle.vim'

" Recommended to install
" After install, turn shell ~/.vim/bundle/vimproc, (n,g)make -f your_machines_makefile
NeoBundle 'Shougo/vimproc', {
            \  'build' : {
            \      'mac' : 'make -f make_mac.mak',
            \      },
            \  }
"
" My Bundles here:
"
" Note: You don't set neobundle setting in .gvimrc!
" Original repos on github
NeoBundle 'tpope/vim-fugitive'
NeoBundle 'hynek/vim-python-pep8-indent'
NeoBundle 'nvie/vim-flake8'
NeoBundle 'scrooloose/nerdtree'
NeoBundle 'kien/ctrlp.vim'
NeoBundle 'ervandew/supertab'
NeoBundle 'bronson/vim-trailing-whitespace'
NeoBundle 'scrooloose/syntastic'
NeoBundle 'ardagnir/united-front'
NeoBundle 'drmikehenry/vim-extline'
NeoBundle 'joonty/vdebug.git'
NeoBundle 'xolox/vim-misc'
NeoBundle 'majutsushi/tagbar'
NeoBundle 'arnaud-lb/vim-php-namespace'
NeoBundle 'adoy/vim-php-refactoring-toolbox'
NeoBundle 'docteurklein/vim-symfony'
NeoBundle 'christoomey/vim-tmux-navigator'
NeoBundle 'vim-scripts/tComment'
NeoBundle 'tobyS/pdv'
NeoBundle 'tobyS/vmustache'
NeoBundle 'tomtom/tlib_vim'
NeoBundle 'docteurklein/php-getter-setter.vim'
NeoBundle 'vim-scripts/xml.vim'
NeoBundle 'darfink/vim-plist'
NeoBundle 'StanAngeloff/php.vim'
NeoBundle 'mileszs/ack.vim'
NeoBundle 'evidens/vim-twig'
NeoBundle 'eraserhd/vim-ios/'
NeoBundle 'b4winckler/vim-objc'
NeoBundle 'Shougo/vimproc.vim'
NeoBundle 'aquach/vim-http-client'
NeoBundle 'vim-scripts/SQLComplete.vim'

call neobundle#end()
"
" Brief help
" :NeoBundleList          - list configured bundles
" :NeoBundleInstall(!)    - install(update) bundles
" :NeoBundleClean(!)      - confirm(or auto-approve) removal of unused bundles

filetype plugin indent on     " Required!
filetype plugin on
syntax on

set omnifunc=syntaxcomplete#Complete

" Installation check.
NeoBundleCheck

map ,e :e <C-R>=expand("%:p:h") . "/" <CR>
map ,s :sp <C-R>=expand("%:p:h") . "/" <CR>
map ,T :tabnew <C-R>=expand("%:p:h") . "/" <CR>
map ,v :vsp <C-R>=expand("%:p:h") . "/" <CR>
map ,t :tabnew <C-R>=expand("%:p:h") . "/" <CR>
map ,c :TlistOpen<CR>
" ,l -- window local cd to the directory of the open file
map ,l :lcd <C-R>=expand("%:p:h")<CR><CR>

" php-getter-setter remap
map <buffer> <leader>a <Plug>PhpgetsetInsertBothGetterSetter

map <leader>f <C-w>\|
map <leader>F <C-w>=

set tabstop=4
set shiftwidth=4
set softtabstop=4
set backspace=2
set expandtab
set shiftround
" set number
set nowrap
set noswapfile
set hlsearch
set guioptions=gtrLme

let mapleader = ","
let maplocalleader = ";"
nnoremap <leader>t :CtrlPCurWD<cr>
nnoremap <leader>b :CtrlPBuffer<cr>

" run flake8 on python files
autocmd BufWritePost *.py call Flake8()

" auto resize buffers
autocmd VimResized * wincmd =

let g:plist_display_format = 'json'

" =============================================================================
" ctrlp / file search
" =============================================================================
let g:ctrlp_max_files=20000

" =============================================================================
" Vdebug
" =============================================================================
let g:vdebug_keymap = {
            \"eval_visual" : "<Leader>r"
            \}
" =============================================================================
" phpdoc
" =============================================================================
let g:pdv_template_dir = $HOME ."/.vim/bundle/pdv/templates_snip"
inoremap <Leader>p <C-O>:call pdv#DocumentWithSnip()<CR>
noremap <Leader>p :call pdv#DocumentWithSnip()<CR>

" =============================================================================
" syntastic
" =============================================================================
" PHP Code Sniffer binary (default = "phpcs")
let g:phpqa_codesniffer_cmd='/usr/local/Cellar/php55/5.5.18/bin/phpcs'
let g:syntastic_php_phpcs_args="--standard=PSR2"
let g:syntastic_html_tidy_ignore_errors=[" proprietary attribute \"ng-"]
let g:syntastic_html_tidy_ignore_errors = [
    \"trimming empty <i>",
    \"trimming empty <span>",
    \"<input> proprietary attribute \"autocomplete\"",
    \"proprietary attribute \"role\"",
    \"proprietary attribute \"hidden\"",
\]

" =============================================================================
" NERDTree configs
" =============================================================================
let NERDTreeMapUpdir='-'
" let g:NERDTreeDirArrows=0
let NERDTreeShowHidden=1
let g:NERDTreeWinSize=50

" =============================================================================
" flake8 check
" =============================================================================
let g:flake8_ignore="E121,E123"
autocmd BufWritePost *.py call Flake8()

" =============================================================================
" auto completion and other php stuff
" =============================================================================
autocmd FileType php set omnifunc=phpcomplete#CompletePHP
let g:phpcomplete_index_composer_command="composer"
autocmd FileType php set errorformat=%m\ in\ %f\ on\ line\ %l
autocmd FileType php set makeprg=php\ -l\ %
autocmd FileType php set colorcolumn=120
" let g:SuperTabDefaultCompletionType = "<c-x><c-o>"
" let g:SuperTabDefaultCompletionType = 'context'

inoremap <Leader>d <C-O>:call PhpInsertUse()<CR>
noremap <Leader>d :call PhpInsertUse()<CR>

inoremap <Leader>f <C-O>:call PhpExpandClass()<CR>
noremap <Leader>f :call PhpExpandClass()<CR>

map <C-y> :execute ":!"g:symfony_enable_shell_cmd<CR>

" =============================================================================
" PHP QA
" =============================================================================
"
let g:phpqa_codesniffer_args = "--standard=PSR2"
let g:phpqa_messdetector_ruleset = "~/.phpmd_massive.xml"

" =============================================================================
" Overwrite php highlighting
" =============================================================================

function! PhpSyntaxOverride()
    hi! def link phpDocTags  phpDefine
    hi! def link phpDocParam phpType
endfunction

augroup phpSyntaxOverride
    autocmd!
    autocmd FileType php call PhpSyntaxOverride()
augroup END

" =============================================================================
" Keyboard Bindings
" =============================================================================
":set shortmess=a
":set cmdheight=4
nmap <silent> <D-S-Up> <Esc>:wincmd k<CR>
nmap <silent> <D-S-Down> <Esc>:wincmd j<CR>
nmap <silent> <D-S-Left> <Esc>:wincmd h<CR>
nmap <silent> <D-S-Right> <Esc>:wincmd l<CR>
nmap <silent> <D-s> :silent w<Enter>

" =============================================================================
" auto reloading .vimrc
" =============================================================================
augroup reload_vimrc " {
    autocmd!
    autocmd BufWritePost $MYVIMRC source $MYVIMRC
augroup END " }

" =============================================================================
" Clang complete
" =============================================================================
let s:clang_library_path='/Library/Developer/CommandLineTools/usr/lib'
if isdirectory(s:clang_library_path)
    let g:clang_library_path=s:clang_library_path
endif

" au FileType xml exe ":silent %!xmllint --format --recover - 2>/dev/null"
